package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Pawns.Tower;
import com.catholille.rhizome.m1.Players.Player;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {

    @Test
    public void movePawnToCoordinate() {
        Board board = new Board();
        Player player = new Player("Michel", Color.WHITE);
        assertTrue(Board.movePawnToCoordinate(player, Board.findPawnByName("WP0"), new Coordinate(2, 0)));
        assertTrue(Board.movePawnToCoordinate(player, Board.findPawnByName("WP0"), new Coordinate(3, 0)));
        Player player2 = new Player("MichelEnNoir", Color.BLACK);
        assertFalse(Board.movePawnToCoordinate(player2, Board.findPawnByName("WP0"), new Coordinate(2, 0)));
        assertFalse(Board.movePawnToCoordinate(player2, Board.findPawnByName("BP7"), new Coordinate(7, 0)));
        assertTrue(Board.movePawnToCoordinate(player2, Board.findPawnByName("BP7"), new Coordinate(5, 7)));
    }

    @Test
    public void isCheckWhite() {
        Board board = new Board();
        Board.board[7][3] = null;
        Board.board[7][1] = null;
        Board.board[6][4] = null;
        Board.board[7][2] = new Tower(Color.WHITE, new Coordinate(7, 2), 3);
        Assert.assertTrue(board.isCheck());
    }

    @Test
    public void isCheckBlack() {
        Board board = new Board();
        Board.board[0][3] = null;
        Board.board[0][1] = null;
        Board.board[1][4] = null;
        Board.board[0][2] = new Tower(Color.BLACK, new Coordinate(0, 2), 3);
        Assert.assertTrue(board.isCheck());
    }

    @Test
    public void isCheckFalse() {
        Board board = new Board();
        Assert.assertFalse(board.isCheck());
    }

    @Test
    public void findPawnByName() {
        assertEquals(Board.board[0][0], Board.findPawnByName("WR0"));
    }
}