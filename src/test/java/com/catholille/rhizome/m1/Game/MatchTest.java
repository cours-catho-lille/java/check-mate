package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Players.Player;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MatchTest {

    /**
     * Run a test game simulating player moves based on Fool's mate strategy
     * Fool's mate here : https://en.wikipedia.org/wiki/Fool%27s_mate
     */
    @Test
    public void launchPvp() {
        Board board = new Board();

        Player white = new Player("White test", Color.WHITE);
        Player black = new Player("Black test", Color.BLACK);

        Board.movePawnToCoordinate(white, Board.findPawnByName("WP5"), new Coordinate(2, 5));
        Board.movePawnToCoordinate(black, Board.findPawnByName("BP4"), new Coordinate(4, 4));
        Board.movePawnToCoordinate(white, Board.findPawnByName("WP6"), new Coordinate(3, 6));
        Board.movePawnToCoordinate(black, Board.findPawnByName("BQ"), new Coordinate(3, 7));
        assertTrue(board.isCheck());
    }
}