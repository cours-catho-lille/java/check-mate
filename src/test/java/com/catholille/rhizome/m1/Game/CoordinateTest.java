package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Pawns.Tower;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CoordinateTest {

    @Test
    public void isOnBoard() {
        Coordinate coordinate1 = new Coordinate(2, 0);
        assertTrue(coordinate1.isOnBoard());
        Coordinate coordinate2 = new Coordinate(-1, 0);
        Assert.assertFalse(coordinate2.isOnBoard());
        Coordinate coordinate3 = new Coordinate(0, -8);
        Assert.assertFalse(coordinate3.isOnBoard());
    }

    @Test
    public void equals() {
        Coordinate coordinate1 = new Coordinate(2, 3);
        Coordinate coordinate2 = new Coordinate(2, 3);
        assertEquals(coordinate1, coordinate2);
    }

    @Test
    public void testToString() {
        Coordinate coordinate = new Coordinate(0, 0);
        assertTrue(coordinate.toString().equals("A1")
                || coordinate.toString().equals("(1,1)"));
    }

    @Test
    public void enterCoordinate() {
        Coordinate coordinate = new Coordinate(0, 0);
        System.setIn(new ByteArrayInputStream("1\n1\n".getBytes()));
        Coordinate entered = Coordinate.enterCoordinate();
        assertEquals(coordinate, entered);
    }

    @Test
    public void isCoordinateCheck() {
        Board board = new Board();
        Board.board[7][3] = null;
        Board.board[7][1] = null;
        Board.board[6][4] = null;
        Board.board[7][2] = new Tower(Color.WHITE, new Coordinate(7, 2), 3);
        assertTrue(new Coordinate(7, 3).isCoordinateCheck(Color.BLACK));
        assertTrue(new Coordinate(7, 4).isCoordinateCheck(Color.BLACK));
        Assert.assertFalse(new Coordinate(6, 4).isCoordinateCheck(Color.BLACK));
    }
}