package com.catholille.rhizome.m1.Players;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Utils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void playTurn() {
        //TODO: I don't understand why this doesn't work.
        Board board = new Board();
        Player player = new Player("Michel", Color.WHITE);
        Utils.scanner = null;
        Utils.scanner = new Scanner(System.in);
        System.setIn(new ByteArrayInputStream("WP1\n".getBytes()));
        //player.playTurn();
    }

    @Test
    public void testToString() {
        Player player = new Player("Michel");
        assertEquals("Michel", player.toString());
    }
}