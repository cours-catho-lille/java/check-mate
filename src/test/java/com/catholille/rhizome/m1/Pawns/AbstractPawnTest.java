package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Coordinate;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbstractPawnTest {

    @Test
    public void isMovementPossibleTrue() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(6, 0), 1);
        assertTrue(pawn.isMovementPossible(new Coordinate(5, 0), Color.BLACK));
    }

    @Test
    public void isMovementPossibleNotOwner() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        assertFalse(pawn.isMovementPossible(new Coordinate(1, 0), Color.WHITE));
    }

    @Test
    public void isMovementPossibleOutOfGrid() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        assertFalse(pawn.isMovementPossible(new Coordinate(-1, 0), Color.WHITE));
    }

    @Test
    public void isSameTeam() {
        Pawn white = new Pawn(Color.WHITE, new Coordinate(0, 0), 1);
        Pawn black = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        assertFalse(white.isSameTeam(black));
        assertTrue(white.isSameTeam(white));
    }

    @Test
    public void testToString() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        assertEquals("\u265F", pawn.toString());
        pawn = new Pawn(Color.WHITE, new Coordinate(0, 0), 1);
        assertEquals("\u2659", pawn.toString());
    }

    @Test
    public void setCoordinate() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        pawn.setCoordinate(new Coordinate(7, 1));
        assertEquals(new Coordinate(7, 1), pawn.coordinate);
    }

    @Test
    public void setCoordinateString() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        pawn.setCoordinate(7, 1);
        assertEquals(new Coordinate(7, 1), pawn.coordinate);
    }

    @Test
    public void getCoordinate() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        Coordinate coordinate = new Coordinate(0, 0);
        assertEquals(pawn.getCoordinate(), coordinate);
    }

    @Test
    public void getName() {
        Pawn pawn = new Pawn(Color.BLACK, new Coordinate(0, 0), 1);
        assertEquals("BP1", pawn.getName());
    }
}