package com.catholille.rhizome.m1.Pawns;

/**
 * The possible Colors for pawns & players
 */
public enum Color {
    WHITE, BLACK
}
