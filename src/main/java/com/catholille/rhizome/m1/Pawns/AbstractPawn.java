package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public abstract class AbstractPawn {
    String name = null;
    Coordinate coordinate = null;
    ArrayList<Coordinate> possibleMoves = new ArrayList<Coordinate>(){};
    Color color = null;
    String display = null;

    public String getName() {
        return name;
    }

    /**
     * Check if moving this pawn to a destination is possible
     * @param destination The Coordinate on which we want to move the pawn
     * @return bool
     */
    public boolean isMovementPossible(Coordinate destination, Color playerColor) {
        boolean possible = false;
        if (getPossibleMoves().contains(destination)) possible = true;
        System.out.println(possibleMoves);
        return (playerColor.equals(color) && destination.isOnBoard() && possible);
    }

    /**
     * Compare the current pawn's color with the targeted pawn
     * @param pawn The targeted pawn
     * @return bool
     */
    public boolean isSameTeam(AbstractPawn pawn) {
        return (pawn.color == color);
    }


    /**
     * Abstract method to calculate possible moves of this specific pawn.
     * @return ArrayList A list of possible coordinates.
     */
    public abstract ArrayList<Coordinate> getPossibleMoves();

    @Override
    public String toString() {
        return display;
    }

    /**
     * Set the pawn coordinate.
     *
     * @param coordinate The new Coordinate
     */
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * Set Pawn's Coordinate based on x y
     * @param x Position X on the grid
     * @param y Position Y on the grid
     */
    public void setCoordinate(int x, int y) {
        this.coordinate.x = x;
        this.coordinate.y = y;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Color getColor() {
        return color;
    }
}
