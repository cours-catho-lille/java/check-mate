package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class Bishop extends AbstractPawn {

    public Bishop(Color color, Coordinate originPosition, int id) {
        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BB" + id;
            display = "\u265D";
        } else {
            name = "WB" + id;
            display = "\u2657";
        }
    }

    @Override
    public ArrayList<Coordinate> getPossibleMoves() {

        possibleMoves.clear();
        AbstractPawn pawn;

        Coordinate newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x + 1, newCoordinate.y + 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x + 1, newCoordinate.y - 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x - 1, newCoordinate.y - 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x - 1, newCoordinate.y + 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        return possibleMoves;
    }
}
