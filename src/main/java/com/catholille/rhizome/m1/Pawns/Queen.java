package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class Queen extends AbstractPawn {

    public Queen(Color color, Coordinate originPosition) {
        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BQ";
            display = "\u265B";
        } else {
            name = "WQ";
            display = "\u2655";
        }
    }

    @Override
    public ArrayList<Coordinate> getPossibleMoves() {
        possibleMoves.clear();
        AbstractPawn pawn;

        Coordinate newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x + 1, newCoordinate.y + 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x + 1, newCoordinate.y - 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x - 1, newCoordinate.y - 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        newCoordinate = coordinate;
        while (newCoordinate.isOnBoard()) {
            newCoordinate = new Coordinate(newCoordinate.x - 1, newCoordinate.y + 1);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        for (int i = coordinate.x + 1; i < 8; i++) {
            newCoordinate = new Coordinate(i, coordinate.y);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.x - 1; i >= 0; i--) {
            newCoordinate = new Coordinate(i, coordinate.y);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.y + 1; i < 8; i++) {
            newCoordinate = new Coordinate(coordinate.x, i);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.y - 1; i >= 0; i--) {
            newCoordinate = new Coordinate(coordinate.x, i);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }

        return possibleMoves;
    }
}
