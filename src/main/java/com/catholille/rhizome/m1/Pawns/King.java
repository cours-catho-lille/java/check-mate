package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class King extends AbstractPawn {
    public King(Color color, Coordinate originPosition) {
        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BK";
            display = "\u265A";
        } else {
            name = "WK";
            display = "\u2654";
        }
    }

    @Override
    public ArrayList<Coordinate> getPossibleMoves() {
        possibleMoves.clear();
        AbstractPawn pawn;
        Coordinate newCoordinate;

        // Straight
        newCoordinate = new Coordinate(coordinate.x + 1, coordinate.y);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 1, coordinate.y);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x, coordinate.y + 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x, coordinate.y - 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        //Diagonals
        newCoordinate = new Coordinate(coordinate.x - 1, coordinate.y - 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x + 1, coordinate.y - 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x + 1, coordinate.y + 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 1, coordinate.y + 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard() && !newCoordinate.isCoordinateCheck(color)) possibleMoves.add(newCoordinate);
        }
        return possibleMoves;
    }
}
