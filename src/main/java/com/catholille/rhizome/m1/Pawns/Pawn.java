package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class Pawn extends AbstractPawn {
    public Pawn(Color color, Coordinate originPosition, int id) {

        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BP" + id;
            display = "\u265F";
        } else {
            name = "WP" + id;
            display = "\u2659";
        }
    }

    @Override
    public ArrayList<Coordinate> getPossibleMoves() {
        possibleMoves.clear();
        Coordinate desireMoveWhite = new Coordinate(this.coordinate.x + 1, this.coordinate.y);
        Coordinate desireMoveBlack = new Coordinate(this.coordinate.x - 1, this.coordinate.y);
        if (color == Color.WHITE && Board.getPawnAtCoordinate(desireMoveWhite) == null) {
            possibleMoves.add(desireMoveWhite);
            if (coordinate.x == 1){
                possibleMoves.add(new Coordinate(this.coordinate.x + 2, this.coordinate.y));
            }
        } else if (color == Color.BLACK && Board.getPawnAtCoordinate(desireMoveBlack) == null){
            possibleMoves.add(desireMoveBlack);
            if (this.coordinate.x == 6){
                possibleMoves.add(new Coordinate(this.coordinate.x - 2, this.coordinate.y));
            }
        }
        if (isAbleToAttack() != null) possibleMoves.addAll(isAbleToAttack());
        return possibleMoves;
    }

    /**
     * Check if the pawn is able to attack an enemy pawn on sides.
     *
     * @return ArrayList The List of Coordinates attackables.
     */
    private ArrayList<Coordinate> isAbleToAttack() {
        ArrayList<Coordinate> coordinateToAttack = new ArrayList<>();
        AbstractPawn pawn;
        if (color == Color.WHITE){
            pawn =  Board.getPawnAtCoordinate(new Coordinate(coordinate.x + 1, coordinate.y + 1));
            if (pawn != null && !isSameTeam(pawn)){
                coordinateToAttack.add(pawn.coordinate);
            }
            pawn =  Board.getPawnAtCoordinate(new Coordinate(coordinate.x + 1, coordinate.y - 1));
            if (pawn != null && !isSameTeam(pawn)){
                coordinateToAttack.add(pawn.coordinate);
            }
        } else if (color == Color.BLACK){
            pawn =  Board.getPawnAtCoordinate(new Coordinate(coordinate.x - 1, coordinate.y + 1));
            if (pawn != null && !isSameTeam(pawn)){
                coordinateToAttack.add(pawn.coordinate);
            }
            pawn =  Board.getPawnAtCoordinate(new Coordinate(coordinate.x - 1, coordinate.y - 1));
            if (pawn != null && !isSameTeam(pawn)){
                coordinateToAttack.add(pawn.coordinate);
            }
        }

        return coordinateToAttack;
    }
}
