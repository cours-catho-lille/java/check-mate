package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class Knight extends AbstractPawn {
    public Knight(Color color, Coordinate originPosition, int id) {
        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BN" + id;
            display = "\u265E";
        } else {
            name = "WN" + id;
            display = "\u2658";
        }
    }

    @Override
    public ArrayList<Coordinate> getPossibleMoves() {
        possibleMoves.clear();
        Coordinate newCoordinate;
        AbstractPawn pawn;

        newCoordinate = new Coordinate(coordinate.x + 2, coordinate.y - 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x + 2, coordinate.y + 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 2, coordinate.y + 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 2, coordinate.y - 1);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x + 1, coordinate.y + 2);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 1, coordinate.y + 2);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x + 1, coordinate.y - 2);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        newCoordinate = new Coordinate(coordinate.x - 1, coordinate.y - 2);
        pawn = Board.getPawnAtCoordinate(newCoordinate);
        if (pawn == null || !isSameTeam(pawn)) {
            if (newCoordinate.isOnBoard()) possibleMoves.add(newCoordinate);
        }

        return possibleMoves;
    }
}
