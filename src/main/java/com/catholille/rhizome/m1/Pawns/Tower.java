package com.catholille.rhizome.m1.Pawns;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;

import java.util.ArrayList;

public class Tower extends AbstractPawn {
    public Tower(Color color, Coordinate originPosition, int id) {
        this.color = color;
        this.coordinate = originPosition;
        if (this.color == Color.BLACK) {
            name = "BR" + id;
            display = "\u265C";
        } else {
            name = "WR" + id;
            display = "\u2656";
        }
    }
    @Override
    public ArrayList<Coordinate> getPossibleMoves() {
        possibleMoves.clear();
        Coordinate newCoordinate;
        AbstractPawn pawn;
        for (int i = coordinate.x + 1; i < 8; i++) {
            newCoordinate = new Coordinate(i, coordinate.y);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.x - 1; i >= 0; i--) {
            newCoordinate = new Coordinate(i, coordinate.y);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.y + 1; i < 8; i++) {
            newCoordinate = new Coordinate(coordinate.x, i);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        for (int i = coordinate.y - 1; i >= 0; i--) {
            newCoordinate = new Coordinate(coordinate.x, i);
            pawn = Board.getPawnAtCoordinate(newCoordinate);
            if (pawn == null) {
                possibleMoves.add(newCoordinate);
            } else if (!isSameTeam(pawn)) {
                possibleMoves.add(newCoordinate);
                break;
            } else break;
        }
        return possibleMoves;
    }
}
