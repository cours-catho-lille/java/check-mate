package com.catholille.rhizome.m1.Players;

import com.catholille.rhizome.m1.Game.Board;
import com.catholille.rhizome.m1.Game.Coordinate;
import com.catholille.rhizome.m1.Game.Match;
import com.catholille.rhizome.m1.Pawns.AbstractPawn;
import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Utils;

import java.util.ArrayList;

/**
 * A human player
 */
public class Player extends AbstractPlayer {
    String name = "player";

    public Player(String name) {
        this.name = name;
    }

    public Player(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public void playTurn(){
        AbstractPawn pawn = null;
        ArrayList<Coordinate> possibleMoves = new ArrayList<>();
        while (pawn == null){
            System.out.println("\n\nSelect a pawn by Coordinate : ");
            //pawn =  Board.getPawnAtCoordinate(Coordinate.enterCoordinate());
            pawn = Board.getPawnAtCoordinate(Coordinate.stringToCoordinate(Utils.getUserEntry()));
            //System.out.println("\n\nSelect a pawn by name : ");
            //pawn = Board.findPawnByName(Utils.getUserEntry());
            if (pawn == null) {
                System.out.println("No pawn here !");
            } else if (color != pawn.getColor()) {
                System.out.println("This is NOT your pawn !");
                pawn = null;
            } else {
                possibleMoves = pawn.getPossibleMoves();
                if (possibleMoves.isEmpty()) pawn = null;
            }
        }
        Match.historic += pawn.toString() + " at " + pawn.getCoordinate().toString();

        System.out.println("Selected " + pawn);

        System.out.println("Possible moves are :" + possibleMoves);

        boolean moved = false;
        while (!moved) {
            System.out.println("\nSelect a destination : ");
            //Coordinate destination = Coordinate.enterCoordinate();
            Coordinate destination = Coordinate.stringToCoordinate(Utils.getUserEntry());

            moved = Board.movePawnToCoordinate(this, pawn, destination);
            if (moved) Match.historic += " => " + destination.toString() + "\n";
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
