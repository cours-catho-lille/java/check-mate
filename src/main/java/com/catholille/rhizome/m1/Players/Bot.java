package com.catholille.rhizome.m1.Players;

public class Bot extends AbstractPlayer {
    @Override
    public void playTurn() {
        // TODO
    }

    /**
     * The difficulty of the bot.
     */
    public enum Difficulty {EASY,MEDIUM, HARD}
    Difficulty difficulty;
    //TODO
    //Stategy strategy = 0;

    @Override
    public String toString() {
        return "Bot difficulty " + difficulty;
    }
}
