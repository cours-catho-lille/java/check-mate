package com.catholille.rhizome.m1.Players;

import com.catholille.rhizome.m1.Pawns.Color;

public abstract class AbstractPlayer {
    public Color color;

    /**
     * Play the turn of a player by prompting a pawn and cordinate on which to move.
     */
    public abstract void playTurn();
}
