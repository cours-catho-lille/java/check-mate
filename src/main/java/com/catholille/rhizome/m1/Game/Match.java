package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Players.AbstractPlayer;
import com.catholille.rhizome.m1.Players.Player;
import com.catholille.rhizome.m1.Utils;

public class Match {
    public static String historic = "";
    static boolean victory;
    AbstractPlayer player1;
    AbstractPlayer player2;
    Board board;
    boolean turn = true;

    /**
     * Launch a Player Versus Player game
     */
    public void launchPvp() {
        System.out.println("Player 1, what's your name my boi ? ");
        //player1 = new Player("Michel", Color.WHITE);
        player1 = new Player(Utils.getUserEntry(), Color.WHITE);

        System.out.println("Player 2, how shall we call you ? ");
        //player2 = new Player("Michalak", Color.BLACK);
        player2 = new Player(Utils.getUserEntry(), Color.BLACK);

        System.out.println("/******************************************************/\n" +
                "/********* " + player1 + " VERSUS " + player2 + " ********/\n" +
                "/******************************************************/\n");

        board = new Board();

        do {
            if (turn) {
                board.isCheck();
                System.out.println(player1 + " is playing.");
                player1.playTurn();
                board.isCheck();
                turn = false;
            } else {
                board.isCheck();
                System.out.println(player2 + " is playing.");
                player2.playTurn();
                board.isCheck();
                turn = true;
            }
            System.out.println(board);
        } while (!victory);
        System.out.println("Historic : \n" + historic);
    }


}
