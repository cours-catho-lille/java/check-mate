package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.*;
import com.catholille.rhizome.m1.Players.AbstractPlayer;

public class Board {
    public static AbstractPawn[][] board;
    private King whiteKing = null;
    private King blackKing = null;

    public Board() {
        board = null;
        board = new AbstractPawn[8][8];
        init();
        System.out.println(toString());
    }

    /**
     * Move a pawn of a player to a destination
     *
     * @param player      The owner of the pawn
     * @param pawn        The pawn to move
     * @param destination The destination of the move
     * @return boolean Did the move work
     */
    public static boolean movePawnToCoordinate(AbstractPlayer player, AbstractPawn pawn, Coordinate destination) {
        if (pawn == null) {
            System.out.println("No pawn selected !");
            return false;
        }
        if (pawn.isMovementPossible(destination, player.color)) {
            board[pawn.getCoordinate().x][pawn.getCoordinate().y] = null;
            pawn.setCoordinate(destination);
            board[destination.x][destination.y] = pawn;
            System.out.println("Moved " + pawn + " to " + destination);
            return true;
        }
        System.out.println("You can't move here !");
        return false;
    }

    /**
     * Retrieve a Pawn from Coordinates
     *
     * @param coordinate Coordinate of the searched pawn
     * @return Pawn|null If the pawn exist at this coordinate, return him, else return null
     */
    public static AbstractPawn getPawnAtCoordinate(Coordinate coordinate) {
        if (coordinate.isOnBoard()) {
            return board[coordinate.x][coordinate.y];
        }
        return null;
    }

    /**
     * Initialize the position of the pawns.
     */
    public void init() {
        for (int i = 0; i < 8; i++) board[1][i] = new Pawn(Color.WHITE, new Coordinate(1, i), i);
        for (int i = 0; i < 8; i++) board[6][i] = new Pawn(Color.BLACK, new Coordinate(6, i), i);
        board[0][0] = new Tower(Color.WHITE, new Coordinate(0, 0), 0);
        board[0][7] = new Tower(Color.WHITE, new Coordinate(0, 7), 1);
        board[7][0] = new Tower(Color.BLACK, new Coordinate(7, 0), 0);
        board[7][7] = new Tower(Color.BLACK, new Coordinate(7, 7), 1);
        board[0][1] = new Knight(Color.WHITE, new Coordinate(0, 1), 0);
        board[0][6] = new Knight(Color.WHITE, new Coordinate(0, 6), 1);
        board[7][1] = new Knight(Color.BLACK, new Coordinate(7, 1), 0);
        board[7][6] = new Knight(Color.BLACK, new Coordinate(7, 6), 1);
        board[0][2] = new Bishop(Color.WHITE, new Coordinate(0, 2), 0);
        board[0][5] = new Bishop(Color.WHITE, new Coordinate(0, 5), 1);
        board[7][2] = new Bishop(Color.BLACK, new Coordinate(7, 2), 0);
        board[7][5] = new Bishop(Color.BLACK, new Coordinate(7, 5), 1);
        board[0][4] = new King(Color.WHITE, new Coordinate(0, 4));
        board[7][4] = new King(Color.BLACK, new Coordinate(7, 4));
        board[0][3] = new Queen(Color.WHITE, new Coordinate(0, 3));
        board[7][3] = new Queen(Color.BLACK, new Coordinate(7, 3));
    }

    /**
     * Verify that the kings are check, or checkmate.
     *
     * @return Is one of the king check ?
     */
    public boolean isCheck() {
        findKings();
        AbstractPawn pawn;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                pawn = Board.getPawnAtCoordinate(new Coordinate(i, j));
                if (pawn != null && !pawn.isSameTeam(blackKing) && pawn.getPossibleMoves().contains(blackKing.getCoordinate())) {
                    System.out.println(blackKing + " is in check.");
                    if (blackKing.getPossibleMoves().isEmpty()) Match.victory = blackKing.getPossibleMoves().isEmpty();
                    System.out.println("White wins !!!");
                    return true;
                } else if (pawn != null && !pawn.isSameTeam(whiteKing) && pawn.getPossibleMoves().contains(whiteKing.getCoordinate())) {
                    System.out.println(whiteKing + " is in check.");
                    if (whiteKing.getPossibleMoves().isEmpty()) Match.victory = whiteKing.getPossibleMoves().isEmpty();
                    System.out.println("Black wins !!!");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retrieve Kings objects on the board and refresh Kings defined below.
     */
    public void findKings() {
        AbstractPawn pawn;

        boolean whiteKingFound = false;
        boolean blackKingFound = false;
        for (int i = 0; i < 8; i++) {

            for (int j = 0; j < 8; j++) {
                pawn = Board.getPawnAtCoordinate(new Coordinate(i, j));
                if (pawn instanceof King) {
                    if (pawn.getColor() == Color.WHITE) {
                        whiteKing = (King) pawn;
                        whiteKingFound = true;
                    } else if (pawn.getColor() == Color.BLACK) {
                        blackKing = (King) pawn;
                        blackKingFound = true;
                    }
                }
            }
            if (whiteKingFound && blackKingFound) break;
        }
    }

    /**
     * @param name Name of the pawn. i.e. BR1
     * @return The pawn object
     */
    public static AbstractPawn findPawnByName(String name) {
        AbstractPawn pawn;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                pawn = Board.getPawnAtCoordinate(new Coordinate(i, j));
                if (pawn != null && pawn.getName().equals(name)) return pawn;
            }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder grid = new StringBuilder();
        grid.append("\n| --------------------------------\n");
        for (int i = 7; i >= 0; i--) {
            grid.append(i + 1 + "|");
            for (int j = 0; j < 8; j++) {
                if (board[i][j] != null) {
                    grid.append("  ");
                    grid.append(board[i][j]);
                } else {
                    grid.append("   ");
                }
                grid.append("|");
            }
            grid.append("\n --------------------------------\n");
        }
        grid.append("   A   B   C   D   E   F   G   H\n");
        return String.valueOf(grid);
    }
}
