package com.catholille.rhizome.m1.Game;

import com.catholille.rhizome.m1.Pawns.AbstractPawn;
import com.catholille.rhizome.m1.Pawns.Color;
import com.catholille.rhizome.m1.Pawns.King;
import com.catholille.rhizome.m1.Utils;

public class Coordinate {
    public int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Ask the user to enter a coordinate with a line and column.
     *
     * @return Coordinate The coordinate entered.
     */
    public static Coordinate enterCoordinate() {
        System.out.println("Line : ");
        int X = Integer.parseInt(Utils.getUserEntry()) - 1;
        System.out.println("Column : ");
        int Y = Integer.parseInt(Utils.getUserEntry()) - 1;
        return new Coordinate(X, Y);
    }

    /**
     * Check if a king of a said color is check on destination
     * Note that both kings can't check each others.
     *
     * @param color The Color of the King
     * @return boolean Is the destination check.
     */
    public boolean isCoordinateCheck(Color color) {
        AbstractPawn pawn;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                pawn = Board.getPawnAtCoordinate(new Coordinate(i, j));
                if (pawn != null && !(pawn instanceof King) && pawn.getColor() != color && pawn.getPossibleMoves().contains(this)) {
                    return true;
                }
            }
        return false;
    }

    public static Coordinate stringToCoordinate(String coordinate) {
        char[] coord = coordinate.toUpperCase().toCharArray();
        char letter = coord[0];
        int x = coord[1];
        return new Coordinate(x - 49, (int) letter - 65);
    }

    @Override
    public boolean equals(final Object opponent) {
        return (opponent instanceof Coordinate && ((Coordinate) opponent).x == this.x && ((Coordinate) opponent).y == this.y);
    }

    /**
     * Check if the Coordinate is still on the chess board.
     * @return Is the Coordinate on board
     */
    public boolean isOnBoard() {
        return (x < 8 && x >= 0 && y < 8 && y >= 0);
    }

    @Override
    public String toString() {
        //return "(" + (x + 1) + "," + (y + 1) + ")";
        return "" + (char) (y + 65) + (x + 1);
    }
}
