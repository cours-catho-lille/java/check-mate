package com.catholille.rhizome.m1;

import java.util.Scanner;

public class Utils {
    public static Scanner scanner = new Scanner(System.in);

    /**
     * Prompt a user for an entry.
     *
     * @return String The user entry
     */
    public static String getUserEntry(){
        return scanner.nextLine();
    }
}
